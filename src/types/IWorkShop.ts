import { Base } from './IBase'
import { FormBase } from './IForm'

export interface WorkShopBase extends Base {
    forms: FormBase[]
}