import { Base } from './IBase'
/**
 * basic element in project eg: input
 */
export interface ElementBase extends Base {
    appearance: Appearance;
    control: Control;
}

/**
 * appearance properties
 */
export interface Appearance {
    width: number;
    marginLeft: number;
    marginTop: number;
    height?: number;
    style?: string;
    cssClass?: string; 
}

/**
 * control properties
 */
export interface Control {
    visible: boolean;
}




