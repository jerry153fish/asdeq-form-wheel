import { Base } from './IBase'
import { ElementBase } from './IElementBase'

/**
 * form which contains multiple elements
 */
export interface FormBase extends Base {
    elements: ElementBase[];
}
/**
 * Element array diff type
 */
export enum DiffType {
    REPLACE,
    INSERT,
    DELETE
}
/**
 * patch of element diff
 */
export interface Patch {
    type: DiffType;
    patch: {
        id: string,
        content?: ElementBase
    }
}