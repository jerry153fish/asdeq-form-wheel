/**
 * enum type for all elements all in asdeq form
 */
export enum ElementTpye {
    INPUT,
    TEXTAREA,
    LINE,
    ADDRESS, 
    MAP,
    YOUTUBE,
    FORM,
    WORKSHOP,
    ELEMENT
}
/**
 * Base type for all elements in asdeq form
 * @property id: uuid of initialization
 * @property type: Element Type
 * @property key: hash key of value type object for diff algorithm
 */
export interface Base {
    id: string;
    type: ElementTpye;
    title?: string;
    key: string;
}