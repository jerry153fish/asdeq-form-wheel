import hash = require('object-hash')
/**
 * 
 * @param obj object type
 * @return deep clone of obj
 */
export function deepClone<T extends object> (obj: T): T {
    if(obj === undefined)
        return null
    return JSON.parse(JSON.stringify(obj))
}

/**
 * get unique key from object
 * @param obj object type
 */
export function getHash<T extends object>(obj: T): string {
    if (!obj)
        return null
    return hash.sha1(obj)
}
