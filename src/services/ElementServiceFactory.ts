import { v4 } from 'uuid'
import { Base, ElementTpye } from '../types/IBase'
import { ElementBase } from '../types/IElementBase'
import { AddressService } from '../services/Elements/AddressService'
import { LineService } from '../services/Elements/LineService'
import { ElementService } from '../services/ElementService'
/**
 * services factory class
 */
export class ServiceFactory {
    /**
     * static factory method of element service generator based on element types
     * @param type element type
     * @return element services
     */
    static createElementServiceByType (type: ElementTpye) : ElementService {
        let base = {
            ...createDefaultBaseType(type),
            appearance: { 
                width: 0, 
                marginLeft: 0,
                marginTop: 0
            },
            control: {
                visible: true
            }
        }
        switch(type) {
            case ElementTpye.ADDRESS:
                let addressType : ElementBase = {
                    ...base,
                }
                return new AddressService(addressType)
            case ElementTpye.LINE:
                let lineType: ElementBase = {
                    ...base
                }
                return new LineService(lineType)
        }
    }
}
/**
 * 
 * @param type element type
 * @return Base type
 */
function createDefaultBaseType (type: ElementTpye) : Base {
    return {
        id: v4(),
        type: type,
        key: null,
        title: `new ${type}`
    }
}

