import { ElementBase} from '../types/IElementBase'

import { BaseService } from './BaseService'
/**
 * abstract element basic service
 */
export abstract class ElementService extends BaseService<ElementBase>{

}