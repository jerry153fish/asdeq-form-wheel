import { Base } from '../types/IBase'
import { deepClone, getHash } from '../utility/utility'
/**
 * abstract class for base service for all base types
 */
export abstract class BaseService<T extends Base>{
    constructor(private elm: T){

    }
    // deep clone current value type
    clone (): T {
        return deepClone<T>(this.elm)
    }
    // check if current value type is same as target for diff algorithm
    same (elm: T) : boolean {
        return getHash(this.elm) === getHash(elm)
    }
    // setter and getter
    get Elm () : T {
        return this.elm
    }
    set Elm (elm : T) {
        this.elm = elm
    }
} 