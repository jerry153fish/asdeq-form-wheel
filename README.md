Reinvented wheel for asdeq web form and enable collaboration with websocket.

### install

```sh
git clone git@gitlab.com:jerry153fish/asdeq-form-wheel.git
cd asdeq-form-wheel 
yarn
```

### test

```sh
npm test
```

### todo list

- Types
    - [x] Base : base for all -- element form workshop
    - [x] Element: basic element eg: input line map etc..
    - [x] Form: Container of elements
    - [x] Workshop: Container of forms 

- Services
    - [x] BaseServices
        - [x] clone
        - [x] same: same type check unchanged
    - [ ] ElementServices
    - [ ] AutocompleteAddressServices: new one I will try to implement
    - [ ] FormServices
        - [ ] addElement
            - [ ] element factory: build element by element type
        - [ ] diff
            - [ ] build patch of two forms -- array diff
        - [ ] patch
            - [ ] update patches
        - [ ] deleteElementById
        - [ ] getElementById
        - [ ] getElements
    - [ ] WorkshopServices : ICacheable, IResposity
        - [ ] addForm
        - [ ] diff
        - [ ] patch
        - [ ] deleteFormById
        - [ ] syncToCache ? just an idea
        - [ ] retrieveFromCache ? just an idea
        - [ ] toJs


- Infrastructure
    - [ ] cache ? indexdb > localforge
    - [ ] repositories ? pulling ? pushing? rxjs

- Components ?
    - [ ] react?
    - [ ] metaEditView
    - [ ] elementRenderView
