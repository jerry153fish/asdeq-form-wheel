import 'mocha';
import { expect } from 'chai';
import { ElementBase } from '../../src/types/IElementBase'
import { ElementTpye } from '../../src/types/IBase'
import { ServiceFactory } from '../../src/services/ElementServiceFactory'
import { AddressService } from '../../src/services/Elements/AddressService'
import { LineService } from '../../src/services/Elements/LineService';

describe('ElementService class test', () => {
    describe('ElementService constructor function test ', () => {
        let test : ElementBase = { 
            id: '123', 
            type: ElementTpye.ADDRESS, 
            key: '0', 
            appearance: {
                width: 0,
                marginLeft: 0,
                marginTop: 0
            },
            control: {
                visible: true
            }
        }
        let result : AddressService = ServiceFactory.createElementServiceByType(ElementTpye.ADDRESS)
        result.Elm = test
        let cloneTest = result.clone()
        let lineService : LineService = ServiceFactory.createElementServiceByType(ElementTpye.LINE)
        it('clone test should have id equal 123', () => {
            expect(cloneTest).haveOwnProperty('id').equal('123');
        });
        it('same test function equal test', () => {
            expect(result.same(test)).equal(true);
        });
        it('same test should not equal test', () => {
            expect(result.same(null)).equal(false);
        });
        it('should create LineService', () => {
            expect(lineService.Elm.type).equal(ElementTpye.LINE)
        })
    })
})