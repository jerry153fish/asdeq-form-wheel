import 'mocha';
import { deepClone, getHash } from '../../src/utility/utility';
import { expect } from 'chai';
import { ElementBase } from '../../src/types/IElementBase'
import { ElementTpye } from '../../src/types/IBase'


describe('Utility deepClone functions', () => {
    describe('deepClone function test object', () => {
        let test = { name: 'jerry' }
        const result = deepClone(test);
        it('should not equal test', () => {
            expect(result).not.equal(test);
        });
        
        it('should have name equal jerry', () => {
            expect(result).haveOwnProperty('name').equal('jerry');
        });
    })

    describe('deepClone function test nested object', () => {
        let test = { name: 'jerry', test: { id: 5 } }
        const result = deepClone(test);
        it('should not equal test', () => {
            expect(result).not.equal(test);
        });
        
        it('should have test object have property id that equals 5', () => {
            expect(result).haveOwnProperty('test').haveOwnProperty('id').equal(5);
        });
    })

    describe('deepClone function test null', () => {
        let test = null
        const result = deepClone(test);
        it('should equal null', () => {
            expect(result).equal(null);
        });
    })

    describe('deepClone function test undefined', () => {
        let test = undefined
        const result = deepClone(test);
        it('should equal null', () => {
            expect(result).equal(null);
        });
    })
});

describe('Utility getHash functions', () => {
    describe('getHash function ElementBase object', () => {
        let test : ElementBase = { 
            id: '123', 
            type: ElementTpye.LINE, 
            key: '0', 
            appearance: {
                width: 0,
                marginLeft: 0,
                marginTop: 0
            },
            control: {
                visible: true
            }
        }
        let test1 : ElementBase = { 
            id: '1231', 
            type: ElementTpye.LINE, 
            key: '0', 
            appearance: {
                width: 0,
                marginLeft: 0,
                marginTop: 0
            },
            control: {
                visible: true
            }
        }

        let test2 : ElementBase = { 
            id: '1231', 
            type: ElementTpye.LINE, 
            key: '0', 
            appearance: {
                width: 0,
                marginLeft: 0,
                marginTop: 0
            },
            control: {
                visible: true
            }
        }
        const result = getHash(test);
        const result1 = getHash(test1);
        const result2 = getHash(test2);
        it('result should not equal result1', () => {
            expect(result).not.equal(result1);
        });
        
        it('result1 should equal result2', () => {
            expect(result1).equal(result2);
        });
    })

    describe('getHash null undefined test object', () => {
        let test : ElementBase = null
        let test1 : ElementBase = undefined

        const result = getHash(test);
        const result1 = getHash(test1);
        it('hash null should equal hash undefined', () => {
            expect(result).equal(result1);
        });
        
        it('result should equal null', () => {
            expect(result).equal(null);
        });
    })
});